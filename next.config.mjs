/** @type {import('next').NextConfig} */
const nextConfig = {
  images: {
    domains: ["localhost", "strapi-iurp.onrender.com"],
  },
};

export default nextConfig;
