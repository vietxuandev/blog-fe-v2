import { Suspense, type PropsWithChildren } from "react";
import { AppRouterCacheProvider } from "@mui/material-nextjs/v14-appRouter";
import { ThemeProvider } from "@mui/material/styles";
import CssBaseline from "@mui/material/CssBaseline";
import theme from "@/styles/theme";
import { type Metadata } from "next";
import { fetcher } from "@/lib";
import {
  GlobalDocument,
  GlobalQuery,
  GlobalQueryVariables,
} from "@/generated/graphql";
import { Providers } from "./providers";
import { Box, Container, Paper, Toolbar } from "@mui/material";
import { Banner, Footer, ScrollTop, SearchAppBar } from "@/components";

import "@/styles/globals.css";
import "@/styles/content-styles.css";

export async function generateMetadata(): Promise<Metadata> {
  // fetch data
  const global = await fetcher<GlobalQuery, GlobalQueryVariables>(
    GlobalDocument
  )();

  return {
    title: global.global?.data?.attributes?.defaultSeo.metaTitle,
    applicationName: global.global?.data?.attributes?.siteName,
    description: global.global?.data?.attributes?.defaultSeo.metaDescription,
    openGraph: {
      images: global.global?.data?.attributes?.defaultSeo.shareImage?.data
        ?.attributes?.url
        ? [
            global.global.data.attributes.defaultSeo.shareImage.data.attributes
              .url,
          ]
        : [],
    },
  };
}

export default function RootLayout({
  children,
  window,
}: PropsWithChildren<{ window?: () => Window }>) {
  return (
    <html lang="en">
      <body suppressHydrationWarning>
        <AppRouterCacheProvider options={{ enableCssLayer: true }}>
          <ThemeProvider theme={theme}>
            <CssBaseline />
            <Providers>
              <Box display="flex" flexDirection="column" minHeight="100vh">
                <Suspense fallback={null}>
                  <SearchAppBar window={window} />
                </Suspense>
                <Toolbar id="back-to-top-anchor" />
                <Banner />
                <Container maxWidth="xl" sx={{ px: 0 }}>
                  <Paper elevation={0} sx={{ p: 2 }}>
                    {children}
                  </Paper>
                </Container>
                <ScrollTop window={window} />
                <Footer />
              </Box>
            </Providers>
          </ThemeProvider>
        </AppRouterCacheProvider>
      </body>
    </html>
  );
}
