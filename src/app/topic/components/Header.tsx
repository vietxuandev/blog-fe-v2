import { NextImage } from "@/components/NextImage";
import {
  TopicsDocument,
  TopicsQuery,
  TopicsQueryVariables,
} from "@/generated/graphql";
import { fetcher } from "@/lib";
import { Box, Card, Typography } from "@mui/material";

type HeaderProps = {
  slug: string;
};

export async function Header({ slug }: HeaderProps) {
  const data = await fetcher<TopicsQuery, TopicsQueryVariables>(
    TopicsDocument,
    {
      filters: {
        slug: {
          eq: slug,
        },
      },
    }
  )();

  return (
    data.topics?.data[0].attributes && (
      <>
        <Card sx={{ position: "relative", pb: "50%" }}>
          <NextImage image={data.topics.data[0].attributes.image.data} />
        </Card>
        <Box mb={2}>
          <Typography mt={2} variant="h5" component="h5" fontWeight="bold">
            {data.topics.data[0].attributes.title}
          </Typography>
          <Typography variant="body2">
            {data.topics.data[0].attributes.description}
          </Typography>
        </Box>
      </>
    )
  );
}
