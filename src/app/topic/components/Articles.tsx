import { ArticleCard } from "@/components/ArticleCard";
import {
  ArticlesDocument,
  ArticlesQuery,
  ArticlesQueryVariables,
} from "@/generated/graphql";
import { fetcher } from "@/lib";
import { Grid } from "@mui/material";

export default async function Articles() {
  const data = await fetcher<ArticlesQuery, ArticlesQueryVariables>(
    ArticlesDocument
  )();

  return data.articles?.data.map((article) => (
    <Grid key={article.id} item xs={12} sm={6} md={4} lg={3}>
      <ArticleCard
        title={article.attributes?.title ?? ""}
        description={article.attributes?.description ?? ""}
        href={`/article/${article.attributes?.slug ?? ""}`}
        image={article.attributes?.image.data}
        publishedAt={article.attributes?.publishedAt}
      />
    </Grid>
  ));
}
