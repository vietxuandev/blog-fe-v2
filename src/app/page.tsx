import { Suspense } from "react";
import Typography from "@mui/material/Typography";

import {
  HomepageDocument,
  HomepageQuery,
  HomepageQueryVariables,
} from "@/generated/graphql";
import { fetcher } from "@/lib";
import { type Metadata, type ResolvingMetadata } from "next";
import { Chaps, Menu } from "./components";
import { Grid } from "@mui/material";
import { ArticleCard } from "@/components/ArticleCard";

type Props = {};

export async function generateMetadata(
  _: Props,
  parent: ResolvingMetadata
): Promise<Metadata> {
  const home = await fetcher<HomepageQuery, HomepageQueryVariables>(
    HomepageDocument
  )();

  const globalMetadata = await parent;

  return {
    title: `${home.homepage?.data?.attributes?.seo?.metaTitle} | ${globalMetadata.applicationName}`,
    description: home.homepage?.data?.attributes?.seo?.metaDescription,
    openGraph: {
      images: [
        ...(home.homepage?.data?.attributes?.seo?.shareImage?.data?.attributes
          ?.url
          ? [home.homepage.data.attributes.seo.shareImage.data.attributes.url]
          : []),
        ...(globalMetadata.openGraph?.images
          ? globalMetadata.openGraph.images
          : []),
      ],
    },
  };
}

export default async function Home() {
  return (
    <Grid container spacing={3}>
      <Grid item sm={12} md={4}>
        <Typography mt={2} variant="h5" component="h5" mb={1} fontWeight="bold">
          Chương
        </Typography>
        <Suspense fallback={<p>loading...</p>}>
          <Menu />
        </Suspense>
      </Grid>
      <Grid item sm={12} md={8} width="100%">
        <Typography mt={2} variant="h5" component="h5" mb={1} fontWeight="bold">
          Danh sách chương
        </Typography>
        <Grid container spacing={2}>
          <Suspense
            fallback={Array(12)
              .fill(null)
              .map((_, index) => (
                <Grid key={index} item xs={12} md={6} lg={4}>
                  <ArticleCard isLoading />
                </Grid>
              ))}
          >
            <Chaps />
          </Suspense>
        </Grid>
      </Grid>
    </Grid>
  );
}
