import { ArticleCard } from "@/components/ArticleCard";
import {
  ChapsDocument,
  ChapsQuery,
  ChapsQueryVariables,
} from "@/generated/graphql";
import { fetcher } from "@/lib";
import { Grid } from "@mui/material";

export async function Chaps() {
  const data = await fetcher<ChapsQuery, ChapsQueryVariables>(ChapsDocument)();

  return data.chaps?.data.map((chap) => (
    <Grid key={chap.id} item xs={12} md={6} lg={4}>
      <ArticleCard
        title={chap.attributes?.title ?? ""}
        description={chap.attributes?.description ?? ""}
        href={`/chap/${chap.attributes?.slug ?? ""}`}
        image={chap.attributes?.image.data}
        publishedAt={chap.attributes?.publishedAt}
      />
    </Grid>
  ));
}
