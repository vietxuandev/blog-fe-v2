import {
  MenuDocument,
  MenuQuery,
  MenuQueryVariables,
} from "@/generated/graphql";
import { fetcher } from "@/lib";
import { ExpandMore } from "@mui/icons-material";
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  List,
  ListItem,
  ListItemButton,
  ListItemText,
  Typography,
} from "@mui/material";
import Link from "next/link";
import React from "react";

export async function Menu() {
  const data = await fetcher<MenuQuery, MenuQueryVariables>(MenuDocument)();

  return data?.chaps?.data.map((chap) => (
    <Accordion key={chap.attributes?.slug}>
      <AccordionSummary
        expandIcon={<ExpandMore />}
        aria-controls="panel1a-content"
        id="panel1a-header"
      >
        <Typography whiteSpace="normal">{chap.attributes?.title}</Typography>
      </AccordionSummary>
      <AccordionDetails>
        <Typography variant="h6" component="h6" fontWeight="bold">
          Chủ đề
        </Typography>
        {chap.attributes?.topics?.data.map((topic) => (
          <Accordion key={topic.attributes?.slug}>
            <AccordionSummary
              expandIcon={<ExpandMore />}
              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              <Typography whiteSpace="pre-wrap">
                {topic.attributes?.title}
              </Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Typography fontWeight="bold">Bài viết</Typography>
              <List sx={{ p: 0 }}>
                {topic.attributes?.articles?.data.map((article) => (
                  <ListItem
                    key={article.attributes?.slug}
                    disablePadding
                    divider
                  >
                    <ListItemButton
                      LinkComponent={Link}
                      href={`/article/${article.attributes?.slug ?? ""}`}
                    >
                      <ListItemText primary={article.attributes?.title} />
                    </ListItemButton>
                  </ListItem>
                ))}
              </List>
            </AccordionDetails>
          </Accordion>
        ))}
      </AccordionDetails>
    </Accordion>
  ));
}
