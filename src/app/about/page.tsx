import { Typography } from "@mui/material";

import { CkContent } from "@/components/CkContent";
import {
  AboutDocument,
  AboutQuery,
  AboutQueryVariables,
} from "@/generated/graphql";
import { fetcher } from "@/lib";

export default async function About() {
  const data = await fetcher<AboutQuery, AboutQueryVariables>(AboutDocument)();

  return (
    <>
      <Typography
        mt={2}
        variant="h4"
        component="h4"
        sx={{ textAlign: "center" }}
        fontWeight="bold"
      >
        Giới thiệu
      </Typography>
      <CkContent content={data?.about?.data?.attributes?.content} />
    </>
  );
}
