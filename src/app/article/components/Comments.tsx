"use client";

import { Box, Button, List, Paper, TextField, Typography } from "@mui/material";
import { SubmitHandler, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import {
  useArticlesQuery,
  useCreateCommentMutation,
  useInfiniteCommentsQuery,
} from "@/generated/graphql";
import { useInfinityScroll } from "@/hooks";
import { CommentListItem } from "./CommentListItem";
import { getNextPageParamFunc } from "@/lib/getNextPageParam";
import { defaultVariablesWithSort } from "@/constants";

type Comment = { name: string; content: string };

const schema = yup
  .object({
    name: yup.string().required("Vui lòng nhập tên"),
    content: yup.string().required("Vui lòng nhập nội dung bình luận"),
  })
  .required();

type CommentsProps = {
  slug: string;
};

export function Comments({ slug }: CommentsProps) {
  const { data } = useArticlesQuery({
    filters: {
      slug: {
        eq: slug,
      },
    },
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm<Comment>({
    resolver: yupResolver(schema),
  });

  const {
    data: commentsData,
    refetch,
    fetchNextPage,
    isFetching,
    isLoading,
  } = useInfiniteCommentsQuery(
    {
      ...defaultVariablesWithSort,
      filters: {
        article: {
          slug: { eq: slug },
        },
      },
    },
    {
      getNextPageParam: (lastPage) =>
        getNextPageParamFunc(lastPage.comments?.meta.pagination),
    }
  );

  const { mutate } = useCreateCommentMutation();

  const onSubmit: SubmitHandler<Comment> = (variables) => {
    mutate(
      {
        data: { ...variables, article: data?.articles?.data?.[0].id },
      },
      {
        onSuccess: () => {
          refetch();
          reset();
        },
      }
    );
  };

  const ref = useInfinityScroll(fetchNextPage);

  return (
    <Paper sx={{ mt: 2 }}>
      <Box
        component="form"
        onSubmit={handleSubmit(onSubmit)}
        pt={2}
        px={2}
        display="flex"
        flexDirection="column"
      >
        <Typography fontWeight="bold">Bình luận</Typography>
        <TextField
          label="Tên"
          fullWidth
          margin="normal"
          {...register("name")}
          error={Boolean(errors.name)}
          helperText={errors.name?.message}
          size="small"
        />
        <TextField
          label="Nội dung"
          fullWidth
          margin="normal"
          {...register("content")}
          error={Boolean(errors.content)}
          helperText={errors.content?.message}
          multiline
          size="small"
          rows={4}
        />
        <Button variant="contained" type="submit" sx={{ ml: "auto" }}>
          Bình luận
        </Button>
      </Box>
      <List>
        {commentsData?.pages.map((page) =>
          page.comments?.data.map((comment) => (
            <CommentListItem
              key={comment.id}
              name={comment.attributes?.name ?? ""}
              content={comment.attributes?.content ?? ""}
              createdAt={comment.attributes?.createdAt}
            />
          ))
        )}
        {(isLoading || isFetching) &&
          Array(4)
            .fill(null)
            .map((_, index) => <CommentListItem key={index} isLoading />)}
      </List>
      <Box ref={ref} />
    </Paper>
  );
}
