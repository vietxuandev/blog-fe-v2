import {
  ArticlesQuery,
  ArticlesQueryVariables,
  ArticlesDocument,
} from "@/generated/graphql";
import { fetcher } from "@/lib";

import { type Metadata, type ResolvingMetadata } from "next";

import { CkContent } from "../../../components/CkContent";
import { Card, Typography } from "@mui/material";
import { NextImage } from "@/components/NextImage";
import { Comments } from "../components/Comments";

type ArticleDetailProps = {
  params: { slug: string };
};

export async function generateMetadata(
  { params }: ArticleDetailProps,
  parent: ResolvingMetadata
): Promise<Metadata> {
  const data = await fetcher<ArticlesQuery, ArticlesQueryVariables>(
    ArticlesDocument,
    {
      filters: {
        slug: {
          eq: params.slug,
        },
      },
    }
  )();

  const globalMetadata = await parent;

  return {
    title: `${data.articles?.data[0].attributes?.title} | ${globalMetadata.applicationName}`,
    description: data.articles?.data[0].attributes?.description,
    openGraph: {
      images: [
        ...(data.articles?.data[0].attributes?.image.data?.attributes?.url
          ? [data.articles.data[0].attributes.image.data.attributes.url]
          : []),
        ...(globalMetadata.openGraph?.images
          ? globalMetadata.openGraph.images
          : []),
      ],
    },
  };
}

export default async function ArticleDetail({
  params: { slug },
}: ArticleDetailProps) {
  const data = await fetcher<ArticlesQuery, ArticlesQueryVariables>(
    ArticlesDocument,
    {
      filters: {
        slug: {
          eq: slug,
        },
      },
    }
  )();
  return (
    data.articles?.data[0].attributes && (
      <>
        <Card sx={{ position: "relative", pb: "50%" }}>
          <NextImage image={data.articles.data[0].attributes.image.data} />
        </Card>
        <Typography
          mt={2}
          variant="h4"
          component="h4"
          sx={{ textAlign: "center" }}
          fontWeight="bold"
        >
          {data.articles.data[0].attributes.title}
        </Typography>
        <CkContent content={data.articles.data[0].attributes.content} />
        <Comments slug={slug} />
      </>
    )
  );
}
