import { ArticleCard } from "@/components/ArticleCard";
import {
  TopicsDocument,
  TopicsQuery,
  TopicsQueryVariables,
} from "@/generated/graphql";
import { fetcher } from "@/lib";
import { Grid } from "@mui/material";

export default async function Topics() {
  const data = await fetcher<TopicsQuery, TopicsQueryVariables>(
    TopicsDocument
  )();

  return data.topics?.data.map((topic) => (
    <Grid key={topic.id} item xs={12} sm={6} md={4} lg={3}>
      <ArticleCard
        title={topic.attributes?.title ?? ""}
        description={topic.attributes?.description ?? ""}
        href={`/topic/${topic.attributes?.slug ?? ""}`}
        image={topic.attributes?.image.data}
        publishedAt={topic.attributes?.publishedAt}
      />
    </Grid>
  ));
}
