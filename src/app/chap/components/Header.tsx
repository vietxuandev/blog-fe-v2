import { NextImage } from "@/components/NextImage";
import {
  ChapsDocument,
  ChapsQuery,
  ChapsQueryVariables,
} from "@/generated/graphql";
import { fetcher } from "@/lib";
import { Box, Card, Typography } from "@mui/material";

type HeaderProps = {
  slug: string;
};

export async function Header({ slug }: HeaderProps) {
  const data = await fetcher<ChapsQuery, ChapsQueryVariables>(ChapsDocument, {
    filters: {
      slug: {
        eq: slug,
      },
    },
  })();

  return (
    data.chaps?.data[0].attributes && (
      <>
        <Card sx={{ position: "relative", pb: "50%" }}>
          <NextImage image={data.chaps.data[0].attributes.image.data} />
        </Card>
        <Box mb={2}>
          <Typography mt={2} variant="h5" component="h5" fontWeight="bold">
            {data.chaps.data[0].attributes.title}
          </Typography>
          <Typography variant="body2">
            {data.chaps.data[0].attributes.description}
          </Typography>
        </Box>
      </>
    )
  );
}
