import { Grid } from "@mui/material";

import {
  ChapsQuery,
  ChapsQueryVariables,
  ChapsDocument,
} from "@/generated/graphql";
import { fetcher } from "@/lib";

import { Header } from "../components/Header";
import { Suspense } from "react";
import Topics from "../components/Topics";
import { type Metadata, type ResolvingMetadata } from "next";
import { ArticleCard } from "@/components/ArticleCard";

type ChapDetailProps = {
  params: { slug: string };
};

export async function generateMetadata(
  { params }: ChapDetailProps,
  parent: ResolvingMetadata
): Promise<Metadata> {
  const data = await fetcher<ChapsQuery, ChapsQueryVariables>(ChapsDocument, {
    filters: {
      slug: {
        eq: params.slug,
      },
    },
  })();

  const globalMetadata = await parent;

  return {
    title: `${data.chaps?.data[0].attributes?.title} | ${globalMetadata.applicationName}`,
    description: data.chaps?.data[0].attributes?.description,
    openGraph: {
      images: [
        ...(data.chaps?.data[0].attributes?.image.data?.attributes?.url
          ? [data.chaps.data[0].attributes.image.data.attributes.url]
          : []),
        ...(globalMetadata.openGraph?.images
          ? globalMetadata.openGraph.images
          : []),
      ],
    },
  };
}

export default async function ChapDetail({
  params: { slug },
}: ChapDetailProps) {
  return (
    <>
      <Suspense fallback={<p>loading...</p>}>
        <Header slug={slug} />
      </Suspense>
      <Grid container spacing={2}>
        <Suspense
          fallback={Array(12)
            .fill(null)
            .map((_, index) => (
              <Grid key={index} item xs={12} sm={6} md={4} lg={3}>
                <ArticleCard isLoading />
              </Grid>
            ))}
        >
          <Topics />
        </Suspense>
      </Grid>
    </>
  );
}
